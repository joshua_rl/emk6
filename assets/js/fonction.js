$(document).ready(function(){
    $(function(){
        var width = $(window).width();
        
        $(window).resize(function() {
            width = $(window).width();
        });
        
        $("#langue").change(function(){
            $("#form-langue").submit();
        });
        $(".img-nav").click(function(){
           $('#nav-menu').slideToggle('slow');
        });
        $("#img-open").click(function(){
           $('body').css('overflow','hidden');
        });
        $("#img-close").click(function(){
           $('body').css('overflow','auto');
        });

        $(".nav-item").click(function(){
            if(width<=1024){
                $('body').css('overflow','auto');
                $('#nav-menu').slideToggle('slow');
            }
        });
        
    });
});
