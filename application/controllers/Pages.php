<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of accueil
 *
 * @author digistage1
 */
class Pages extends CI_Controller {

    public function view($page = 'home')
    {
        if ( !file_exists(APPPATH.'views/pages/fr/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        
        $data['title'] = ucfirst($page); // Met en Capital la premiere lettre
        $this->load->helper('url_helper'); //helper pour les chemins de pages & images
        $this->load->helper('file'); //helper pour les fichers
        $this->load->helper('form'); //helper pour les formulaires
        
        $langue = $this->input->post('langue');
        if ($langue==NULL){
            $langue ='fr';
        }
        $data['langue']=$langue;
        
        $lanInp = $this->input->post('lanInp');
        if ($lanInp!=NULL){
            $langue = $lanInp;
            $data['langue']=$langue;
        }
        $path = (FCPATH."assets/images/galerie/small/");//Chemin des fichiers dans le repertoire galerie
        $data['lesPhotos']= get_filenames($path);//Recupère les nom de chaque fichiers du chemin path
        
        /*
         *  Inclusion des différentes vues avec les données nécessaires
         *  Une modification de certains éléments peuvent être éffectué
         */
        
        $this->load->view('templates/head', $data);  //head.php
        $this->load->view('templates/header', $data); //header.php
        $this->load->view('pages/'.$langue.'/nav', $data); //nav.php
        $this->load->view('pages/'.$langue.'/'.$page, $data); //home.php
        $this->load->view('pages/'.$langue.'/resellers', $data); //distributor.php
        $this->load->view('pages/'.$langue.'/gallery', $data); //gallery.php
        $this->load->view('pages/'.$langue.'/news', $data); //news.php
        $this->load->view('pages/'.$langue.'/videos', $data); //videos.php
        
        

        $this->load->library('form_validation'); //utilisation du formulaire
        $img_url = base_url("assets/images/icons/ic_error_black/web/ic_error_black_48dp_2x.png"); //chemin vers l'image d'erreur
        $img_error = '<img id="img-mess" class="img-bulle" src="'.$img_url.'">'; //image d'erreur
        
        /*
         *  contrainte des input
         *  @required : le inpu doit être remplis
         *  @max_length : la longueur maximale du champ
         *  @valid_email : verifie si l'email est valide
         */
        
        
        if($lanInp == 'fr'){
            $this->form_validation->set_rules('nom', 'nom', 'required|max_length[30]',
            array('required' => $img_error.'Le %s est requis.',
                  'max_length' => $img_error.'Le %s est trop long.' )); 
        
            $this->form_validation->set_rules('prenom', 'prénom', 'required|max_length[30]',
                array('required' => $img_error.'Le %s est requis.',
                      'max_length' => $img_error.'Le %s est trop long.'));

            $this->form_validation->set_rules('mail', 'email', 'required|valid_email|max_length[100]',
                array('required' => $img_error."L'%s est requis.",
                      'valid_email' => $img_error."L'%s doit contenir un email valide.",
                      'max_length' => $img_error."L'%s est trop long."));    

            $this->form_validation->set_rules('message', 'message', 'required',
                array('required' => $img_error.'Le %s est requis.'));
        }
        
        if($lanInp=='en'){
            $this->form_validation->set_rules('nom', 'nom', 'required|max_length[30]',
            array('required' => $img_error.'The first name field is required.',
                  'max_length' => $img_error.'The first name is too long.' )); 
        
            $this->form_validation->set_rules('prenom', 'prénom', 'required|max_length[30]',
                array('required' => $img_error.'The last name field is required.',
                      'max_length' => $img_error.'The last name is too long.'));

            $this->form_validation->set_rules('mail', 'email', 'required|valid_email|max_length[100]',
                array('required' => $img_error."The email field is required.",
                      'valid_email' => $img_error."The email field must be valid.",
                      'max_length' => $img_error."The email field is too long."));    

            $this->form_validation->set_rules('message', 'message', 'required',
                array('required' => $img_error.'The message field is required.'));
        }
       
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('pages/'.$langue.'/contact', $data); //contact.php
        }
        else
        {
            $this->load->view('pages/'.$langue.'/contact', $data); 
            
            
            $nom = $this->input->post('nom');
            $prenom = $this->input->post('prenom');
            $mail = $this->input->post('mail');
            $message = $this->input->post('message');
            
            $this->load->library('email');

            $this->email->from($mail, $nom);
            $this->email->to('jerome.gallix@ffwlab.com');

            $this->email->subject("Contact from $nom $prenom");
            $this->email->message($message);

            $this->email->send();
            
            $this->load->view('pages/'.$langue.'/formsuccess'); //formsuccess.php
        }
        
        $this->load->view('templates/footer', $data); //footer.php
        
    }
    public function configurateur($page = 'configurateur')
    {
        if ( !file_exists(APPPATH."views/templates/".$page.".php"))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        
        $data['title'] = ucfirst($page); // Met en Capital la premiere lettre
        
        $this->load->view('templates/head', $data);  //head.php
        $this->load->view('templates/header', $data); //header.php
        $this->load->view('templates/configurateur', $data); //configurateur.php
        $this->load->view('templates/footer', $data); //footer.php
    }
        
}
