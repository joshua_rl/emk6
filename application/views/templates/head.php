<!DOCTYPE html>
<!--
header des pages
-->
    <?php 
        $this->load->helper('url_helper');
    ?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>eMK6</title>
        <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel ="stylesheet">
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel ="stylesheet">
        <link href="<?php echo base_url('assets/css/mobile.css'); ?>" media="all and (max-width: 1280px)" rel ="stylesheet">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="<?php echo base_url('assets/js/fonction.js');?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
        
    </head>