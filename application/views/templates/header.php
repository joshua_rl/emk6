<body> 
    <header>
        <form id="form-langue" action="<?php echo site_url();?>" method="POST">
            <select class="form-langue" id="langue" name="langue">
                <option class="select-langue" value="fr" <?php if($langue=='fr'){echo 'selected';} ?>>Français</option>
                <option class="select-langue" value="en" <?php if($langue=='en'){echo 'selected';} ?>>English</option>
            </select>
        </form>
        <img class="img-nav el-mobile" id="img-open" src="<?php echo base_url('assets/images/icons/ic_menu_black/web/ic_menu_black_48dp_2x.png'); ?>" />
        <div class="header-logo">
            <a class="link-logo" href="<?php echo site_url(); ?>">
                <img class="img-logo" src="<?php echo base_url('assets/images/eMk6_logo.png'); ?>" />
            </a>
        </div>
        <a class="link" href="<?php echo site_url(''); ?>">home</a>
    </header>