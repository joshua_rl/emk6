
<h2 class="title" id="galerie">Gallery :</h2>
<div class="text-center gallery">
    <?php
        asort($lesPhotos);
        foreach ($lesPhotos as $unePhoto){
            $img =rtrim($unePhoto,".jpg"); //ex : galerie01            
           ?> 
                <div class="cont-gallery">
                    <img class="img-gallery" data-toggle="modal" data-target="<?php echo "#".$img; ?>" src="<?php echo base_url('assets/images/galerie/small/'.$unePhoto); ?>"/>
                </div>
                <div class="modal fade bd-modal-lg" id="<?php echo $img;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLongTitle"></div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <img class="img-modal" src="<?php echo base_url('assets/images/galerie/large/'.$unePhoto); ?>"/>
                        </div>
                    </div>
                  </div>
                </div>
           <?php
        }
    ?>
</div>


