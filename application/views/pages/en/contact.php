
<div class="banner">
    <div class="info-banner contact">
        Contact
    </div>
    <img id="contact" class="img-banner" src="<?php echo base_url('assets/images/visuel-spec.jpg'); ?>">
</div>


<div class="form-contact">
    <?php
        $this->load->helper('form');
        echo validation_errors();
        echo form_open('#contact', 'id=form-contact'); 
        $img = base_url("assets/images/icons/ic_done_black/web/ic_done_black_32dp_2x.png");
    ?>

    <!--<form class="form-contact" id="form-contact" method="POST">-->
        <div class="inp-nom">
            <label  class="label-contact">First name*</label><br />
            <input id="nom" type="text" name="nom" value="<?php echo set_value('nom');?>" placeholder="Your first name"  >
            <div class="bulle"><?php echo form_error('nom'); ?></div>
        </div>
        <div class="inp-pren">
            <label class="label-contact" >Last name*</label><br />
            <input id="prenom" type="text" name="prenom" value="<?php echo set_value('prenom');?>" placeholder="Your last name" >
            <div class="bulle"><?php echo form_error('prenom'); ?></div>
        </div>
        <div class="inp-mail">
            <label  class="label-mail">Email*</label><br />
            <input id="mail"type="text" name="mail" value="<?php echo set_value('mail');?>" placeholder="Your email" >
            <div class="bulle"><?php echo form_error('mail'); ?></div>
        </div>
        <div class="inp-dest">
            <label class="label-cont" >Message*</label><br />
            <textarea id="message" type="text" name="message" ><?php echo set_value('message');?></textarea>
            <div class="bulle"><?php echo form_error('message'); ?></div>
        </div>
        <input id="btn-valide" style ="background:url('<?php echo $img;?>') no-repeat;" type='submit' value="Send your message"/>
        <input id="lan" type="hidden" name="lanInp" value="en" >
    </form>
</div>
