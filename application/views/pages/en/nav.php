<nav id="nav-menu" class="navbar navbar-light bg-light">
    <img class="img-nav el-mobile" id="img-close" src="<?php echo base_url('assets/images/icons/ic_close_black/web/ic_close_black_1024dp_1x.png'); ?>" />
    <ul class="nav-button">
        <li class="nav-item"><a class="nav-link" href="#home">eMK6</a></li>
        <li class="nav-item"><a class="nav-link" href="#resellers">Resellers</a></li>
        <li class="nav-item"><a class="nav-link" href="#galerie">Gallery</a></li>
        <li class="nav-item"><a class="nav-link" href="#news">News</a></li>
        <li class="nav-item"><a class="nav-link" href="#videos">Videos</a></li>
        <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
    </ul>
</nav>
<div data-spy="scroll" data-target="#nav-menu" data-offset="0">