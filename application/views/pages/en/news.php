
<h2 class="title" id="news">News :</h2>

<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/logo-shell.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">February 2014 :</div>
        <div class="desc-news">Official Pace-car of the Marathon Shell</div>
    </div>
</div>
<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/visuel-actu1.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">April 2015 :</div>
        <div class="desc-news">Salon Top Marques Monaco - Official presentation of David David's Art-car</div>
    </div>
</div>
<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/visuel-actu2.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">December 2017 :</div>
        <div class="desc-news">Exhibition at the Louis Vuitton Vuitton pop-up store in Milan La Rinascente</div>
    </div>
</div>
<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/visuel-actu3.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">January 2018 :</div>
        <div class="desc-news">Exhibition at the Pop-Up store Louis Vuitton in London Selfridges</div>
    </div>
</div>