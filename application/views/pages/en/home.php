

<div id="home" class="banner" >
    <div class="info-banner">
        MK6 CONFIGURATOR<br />
        <a class="link-banner" href="<?php echo site_url('pages/configurateur'); ?>">CLICK HERE</a>        
    </div>
    <img  class="img-banner" src="<?php echo base_url('assets/images/banner-1.jpg'); ?>" >
</div>
    
</div>
<div class="info-home">
    <p>Driving an eMk6 is a lifestyle : a classic and timeless aesthetic, a crucial vehicle, a robust design and proven reliability, and the silence and comfort of the high performance electric motion.</p>
    <div class="list">
        <ul class="list-info left">
            <li>Pure driving pleasure</li>
            <li>Ease of use</li>
            <li>Maintenance free</li>
            
        </ul>
        <ul class="list-info right">
            <li>Battery charge on any standard electric plug (220V or 110V US)</li>
            <!--<li>100km d’autonomie réelle (75km en standard)</li>-->
            <li>Can be driven from the age of 16 (Licence A1)</li>
            <li>L7 Commercial Vehicle registration</li>
        </ul>
    </div>     
</div>
<img class="img-banner" src="<?php echo base_url('assets/images/banner-3.jpg'); ?>">
<div class="car-perf">
    <div class="left">
        <div class="title-dist">Characteristics :</div>
        <ul class='list-dist'>
            <li>Overall length = 316 cm ,Overall width= 145 cm, Overall height =142 cm.</li>
            <li>Seats : 2</li>
            <li>Gross weight  = 865 kg</li>
            <li>Tyres size: 165/60/12</li>
        </ul>
    </div>
    <div class="right">
        <div class="title-dist">Performance :</div>
        <ul class='list-dist'>
            <li>Motor power= 15 kW</li>
            <li>Energy available= from 10 kWh (14 kWh with extended battery pack)</li>
            <li>Top speed : 55 mph</li>
            <li>50 miles true operating range (75 miles with extended battery pack)</li>
            <li>Average charge time: 3 hours for 45 miles range</li>
        </ul>
    </div>
</div>
<img class="img-banner" src="<?php echo base_url('assets/images/banner-2.jpg'); ?>">