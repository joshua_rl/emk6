
<h2 class="title" id="news">Les news :</h2>

<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/logo-shell.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">Février 2014 :</div>
        <div class="desc-news">Pace-car officiel du Marathon Shell</div>
    </div>
</div>
<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/visuel-actu1.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">Avril 2015 :</div>
        <div class="desc-news">Salon Top Marques de Monaco - Présentation officielle de l'Art-car David David</div>
    </div>
</div>
<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/visuel-actu2.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">Décembre 2017 :</div>
        <div class="desc-news">Exposition au Pop-Up store Louis Vuitton Vuitton de Milan La Rinascente</div>
    </div>
</div>
<div class="news">
    <img class="img-news" src="<?php echo base_url('assets/images/visuel-actu3.jpg'); ?>"/>
    <div class='info-news'>
        <div class="date-news">Janvier 2018 :</div>
        <div class="desc-news">Exposition au Pop-Up store Louis Vuitton de Londres Sefridges</div>
    </div>
</div>