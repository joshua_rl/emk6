

<div id="home" class="banner" >
    <div class="info-banner">
        Configurer votre eMK6<br />
        <a class="link-banner" href="<?php echo site_url('pages/configurateur'); ?>">ici</a>        
    </div>
    <img  class="img-banner" src="<?php echo base_url('assets/images/banner-1.jpg'); ?>" >
</div>
    
</div>
<div class="info-home">
    <p>Rouler dans eMK6 c'est afficher un style de vie : une ligne indémodable, le luxe d'un produit manufacturé pour vous en France et à la pointe de la technologie électrique</p>
    <div class="list">
        <ul class="list-info left">
            <li>Plaisir de conduite</li>
            <li>Simplicité d’utilisation</li>
            <li>Sans entretien</li>
            <li>Branchement pour recharge sur toute prise 220V</li>
        </ul>
        <ul class="list-info right">
            <li>100km d’autonomie réelle (75km en standard)</li>
            <li>Conduisible dès 16 ans (Permis A1)</li>
            <li>TVA récupérable</li>
        </ul>
    </div>     
</div>
<img class="img-banner" src="<?php echo base_url('assets/images/banner-3.jpg'); ?>">
<div class="car-perf">
    <div class="left">
        <div class="title-dist">Caractéristiques :</div>
        <ul class='list-dist'>
            <li>Longueur = 316 cm, largeur = 145 cm, hauteur (odm) = 142 cm.</li>
            <li>Nombre de places assises : 2 (TVA récupérable)</li>
            <li>Poids total autorisé en charge = 865 kg</li>
            <li>Dimension des pneumatiques : 165/60/12</li>
        </ul>
    </div>
    <div class="right">
        <div class="title-dist">Performances :</div>
        <ul class='list-dist'>
            <li>Puissance max. = 15 kW</li>
            <li>Energie embarquée = de 10 à 15 kWh</li>
            <li>100km d’autonomie réelle (75km en standard)</li>
            <li>Temps de recharge : 7h en mode normal, 3h en mode rapide</li>
            <li>Vitesse maximale 85 km/h</li>
        </ul>
    </div>
</div>
<img class="img-banner" src="<?php echo base_url('assets/images/banner-2.jpg'); ?>">