
<h2 class="title" id="galerie">La Gallerie :</h2>
<div class="text-center gallery">
    <?php
        foreach ($lesPhotos as $unePhoto){
            $img =rtrim($unePhoto,".jpg"); //ex : galerie01
           ?> 
                
                <div class="cont-gallery">
                    <a href="<?php echo base_url('#'.$unePhoto); ?>" data-toggle="modal" data-target="<?php echo "#".$img; ?>" >
                        <img class="img-gallery"  src="<?php echo base_url('assets/images/galerie/small/'.$unePhoto); ?>"/>
                        <img class="icon-gallery" src="<?php echo base_url('assets/images/icons/ic_control_point_black/web/ic_control_point_black_48dp_1x.png'); ?>"/>
                    </a>
                </div>
               
                <div class="modal fade bd-modal-lg" id="<?php echo $img;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title" id="exampleModalLongTitle"></div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <img class="img-modal" src="<?php echo base_url('assets/images/galerie/large/'.$unePhoto); ?>"/>
                    </div>
                  </div>
                </div>
           <?php
        }
    ?>
</div>


